package com.skyline.courier.net.test;

import com.skyline.courier.net.Handler;
import com.skyline.courier.net.Server;
import com.skyline.courier.net.provider.netty.NettyServer;

public class TestServer {

	public static void main(String[] args) {
		Server server = new NettyServer();
		server.setHandler(new Handler() {
			
			@SuppressWarnings("unchecked")
			@Override
			public <I, O> O process(I input) {
				return (O) input;
			}
		});
		server.bind(12345);
	}

}
