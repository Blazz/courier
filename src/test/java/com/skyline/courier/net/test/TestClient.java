package com.skyline.courier.net.test;

import com.skyline.courier.net.Client;
import com.skyline.courier.net.Connection;
import com.skyline.courier.net.provider.netty.NettyClient;

public class TestClient {

	public static void main(String[] args) throws Throwable {
		Client client = new NettyClient();
		client.setHosts("localhost:12345");
		Connection connection = client.connect();
		String out = connection.send("123");
		System.err.println(out);
		connection = client.connect();
		String out1 = connection.send("1234");
		System.err.println(out1);
		//connection.close();
		//client.close();
		Thread.sleep(40000);
		connection = client.connect();
		System.err.println(connection.send("456"));
	}

}
