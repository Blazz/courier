package com.skyline.courier.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 消息发送客户端基类
 * 
 * 
 * @author wuqh
 * 
 */
public abstract class AbstractMessageSendClient implements MessageSendClient {
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMessageSendClient.class);

	@Override
	public <T> void send(String key, T message, Protocol<T, byte[]> protocol) {
		send(key, message, protocol, false, false);
	}

	@Override
	public <T> void send(String key, T message, Protocol<T, byte[]> protocol, boolean mandatory, boolean direct) {
		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("发送对象[" + message + "]到 [" + key + "] - mandatory [" + mandatory + "] - immediate [" + direct
					+ "]");
		}
		byte[] data = protocol.encode(message);
		sendInternal(key, data, mandatory, direct);
	}

	/**
	 * 消息发生内部实现
	 * 
	 * @param key
	 * @param data
	 * @param mandatory
	 * @param direct
	 */
	protected abstract void sendInternal(String key, byte[] data, boolean mandatory, boolean direct);
}
