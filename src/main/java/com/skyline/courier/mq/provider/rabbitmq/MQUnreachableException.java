package com.skyline.courier.mq.provider.rabbitmq;

/**
 * 无法连接到RabbitMQ消息中间件异常
 * 
 * @author wuqh
 * 
 */
public class MQUnreachableException extends RuntimeException {
	private static final long serialVersionUID = -5722774120801097120L;

	public MQUnreachableException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MQUnreachableException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
