package com.skyline.courier.mq.provider.gearman;

/**
 * 无法连接到Gearman作业服务器异常
 * 
 * @author wuqh
 * 
 */
public class GearmanUnreachableException extends RuntimeException {
	private static final long serialVersionUID = -5722774120801097120L;

	public GearmanUnreachableException(String message, Throwable cause) {
		super(message, cause);
	}

	public GearmanUnreachableException(String message) {
		super(message);
	}

}
