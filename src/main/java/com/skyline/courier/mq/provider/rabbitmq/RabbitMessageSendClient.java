package com.skyline.courier.mq.provider.rabbitmq;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.skyline.courier.mq.QueuedMessageSendClient;

/**
 * RabbitMQ消息发送客户端
 * 
 * @author wuqh
 * 
 */
public class RabbitMessageSendClient extends QueuedMessageSendClient {
	private Channel channel;
	private String exchangeName;

	protected void sendInternal(String key, byte[] data, boolean mandatory, boolean immediate) {
		try {
			channel.basicPublish(exchangeName, key, mandatory, immediate, null, data);
		} catch (IOException e) {
			throw new MQUnreachableException("发布消息失败", e);
		}
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}

}
