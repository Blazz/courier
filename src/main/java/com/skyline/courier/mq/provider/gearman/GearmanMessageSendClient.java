package com.skyline.courier.mq.provider.gearman;

import org.gearman.client.GearmanClient;
import org.gearman.client.GearmanJob;
import org.gearman.client.GearmanJobImpl;

import com.skyline.courier.mq.AbstractMessageSendClient;

/**
 * Gearman消息发送（作业提交）客户端
 * 
 * @author wuqh
 * 
 */
public class GearmanMessageSendClient extends AbstractMessageSendClient {
	private GearmanClient client;

	public void setClient(GearmanClient client) {
		this.client = client;
	}

	@Override
	protected void sendInternal(String key, byte[] data, boolean mandatory, boolean direct) {
		GearmanJob job = GearmanJobImpl.createBackgroundJob(key, data, null);
		client.submit(job);
	}

}
