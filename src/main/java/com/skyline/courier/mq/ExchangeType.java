package com.skyline.courier.mq;

/**
 * 消息交换方式
 * 
 * @author wuqh
 * 
 */
public enum ExchangeType {
	DIRECT, FANOUT, TOPIC;

	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}
}
