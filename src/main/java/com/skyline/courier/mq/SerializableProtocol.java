package com.skyline.courier.mq;

import java.io.Serializable;

import org.apache.commons.lang.SerializationUtils;

/**
 * 基于{@link Serializable}的传输协议
 * 
 * @author wuqh
 * 
 * @param <T>
 */
public class SerializableProtocol<T extends Serializable> implements Protocol<T, byte[]> {

	@Override
	public byte[] encode(Serializable object) {
		return SerializationUtils.serialize(object);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T decode(byte[] data) {
		return (T) SerializationUtils.deserialize(data);
	}

}
