package com.skyline.courier.mq;

/**
 * 消息发送客户端接口
 * 
 * 
 * @author wuqh
 * 
 */
public interface MessageSendClient {
	/**
	 * 发生消息
	 * 
	 * @param key
	 *            路由的key值
	 * @param message
	 *            消息对象
	 * @param protocol
	 *            交换协议方式
	 */
	public <T> void send(String key, T message, Protocol<T, byte[]> protocol);

	/**
	 * 发生消息
	 * 
	 * @param key
	 *            路由的key值
	 * @param message
	 *            消息对象
	 * @param protocol
	 *            交换协议方式
	 * @param mandatory
	 *            消息被发送到交换器并且还未投递到队列（没有绑定器存在）的时候得到通知
	 * @param direct
	 *            没有消费者能够立即处理的时候得到通知
	 */
	public <T> void send(String key, T message, Protocol<T, byte[]> protocol, boolean mandatory, boolean direct);

}
