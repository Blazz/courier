package com.skyline.courier.mq;

/**
 * 传输协议，用于消息对象和传输格式之间的转换
 * 
 * @author wuqh
 * 
 * @param <I>
 * @param <O>
 */
public interface Protocol<I, O> {
	/**
	 * 协议编码，将消息对象转换为传输格式
	 * 
	 * @param object
	 * @return
	 */
	public O encode(I object);

	/**
	 * 协议解码，将传输格式转换为消息对象
	 * 
	 * @param object
	 * @return
	 */
	public I decode(O object);
}
