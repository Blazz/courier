package com.skyline.courier.mq;

/**
 * 消息发送客户端池接口
 * 
 * @author wuqh
 * 
 */
public interface MessageSendClientPool {
	/**
	 * 消息中间件地址，多个地址使用','分割，入"localhost:2345,localhost:3456"
	 * 
	 * @param hosts
	 */
	public void setHost(String hosts);

	/**
	 * 消息交换格式（DIRECT, FANOUT, TOPIC）
	 * 
	 * @param type
	 */
	public void setExchangeType(ExchangeType type);

	/**
	 * 消息交换器名称
	 * 
	 * @param exchangeName
	 */
	public void setExchangeName(String exchangeName);

	/**
	 * 获取发送消息的客户端
	 * 
	 * @return
	 * @throws RuntimeException
	 *             如果无法连接服务器
	 */
	public MessageSendClient getClient();

	/**
	 * 销毁消息发送客户端池
	 * 
	 */
	public void destroy();

}
