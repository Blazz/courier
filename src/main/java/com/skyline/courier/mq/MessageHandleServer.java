package com.skyline.courier.mq;

/**
 * 消息处理服务端接口
 * 
 * @author wuqh
 * 
 */
public interface MessageHandleServer {
	/**
	 * 消息中间件地址，多个地址使用','分割，入"localhost:2345,localhost:3456"
	 * 
	 * @param hosts
	 */
	public void setHost(String hosts);

	/**
	 * 消息交换格式（DIRECT, FANOUT, TOPIC）
	 * 
	 * @param type
	 */
	public void setExchangeType(ExchangeType type);

	/**
	 * 消息交换器名称
	 * 
	 * @param exchangeName
	 */
	public void setExchangeName(String exchangeName);

	/**
	 * 注册消息处理器到消息处理服务端
	 * 
	 * @param key
	 *            路由的key值
	 * @param handler
	 *            消息处理接口
	 * @param protocol
	 *            消息传输协议
	 */
	public void registerMessageHandler(String key, MessageHandler handler, Protocol<?, byte[]> protocol);

	/**
	 * 启动消息处理服务端
	 * 
	 */
	public void start();

	/**
	 * 关闭消息处理服务端
	 * 
	 */
	public void shutdown();
	
	/**
	 * 设置消费者处理线程数
	 * 
	 * @param workThreads
	 */
	public void setWorkThreads(int workThreads);
}
