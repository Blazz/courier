package com.skyline.courier.mq;

import java.util.Map;

/**
 * 消息处理接口，用于服务端处理消息
 * 
 * @author wuqh
 * 
 */
public interface MessageHandler {
	/**
	 * 消息处理
	 * 
	 * @param key
	 *            路由的key值
	 * @param message
	 *            消息对象
	 * @param context
	 *            消息上下文信息
	 */
	public <T> void handle(String key, T message, Map<?, ?> context);
}
