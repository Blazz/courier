package com.skyline.courier.rpc;

import java.util.List;

import com.skyline.common.utils.Assert;
import com.skyline.courier.net.Server;

public class ServiceBootstrap {
	private Server server;
	
	public void setServer(Server server) {
		this.server = server;
	}
	
	public void bootstrap(int port, List<?> servics) throws Exception {
		Assert.notNull(server, "创建RPC失败：服务器未设置");
		
		ServiceRepository serviceRepository = new ServiceRepository();
		serviceRepository.registerServices(servics);
		
		RpcMsgHandler handler = new RpcMsgHandler();
		handler.setRepository(serviceRepository);
		server.setHandler(handler);
		
		server.bind(port);
	}
	
	public void shutdown() {
		if(server == null) {
			return;
		}
		server.shutdown();
	}
}
