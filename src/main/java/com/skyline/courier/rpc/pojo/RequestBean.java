package com.skyline.courier.rpc.pojo;

import java.io.Serializable;

public class RequestBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8038310057904162926L;
	private String serviceName;
	private String methodName;
	private Object[] args;
	
	public String getServiceName() {
		return serviceName;
	}
	
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	public String getMethodName() {
		return methodName;
	}
	
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	public Object[] getArgs() {
		return args;
	}
	
	public void setArgs(Object[] args) {
		this.args = args;
	}
}
