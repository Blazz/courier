package com.skyline.courier.rpc.exception;

public class RpcException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 883013250261263120L;

	public RpcException() {
		super();
	}

	public RpcException(String message, Throwable cause) {
		super(message, cause);
	}

	public RpcException(String message) {
		super(message);
	}

	public RpcException(Throwable cause) {
		super(cause);
	}

}
