package com.skyline.courier.rpc.exception;


/**
 * RPC方法配置异常，通常是Annotation误用导致的异常。
 * 
 * @author wuqh
 * 
 */
public class RpcGenerateException extends Exception {
	private static final long serialVersionUID = -6330147888709232917L;

	public RpcGenerateException() {
		super();

	}

	public RpcGenerateException(String message, Throwable cause) {
		super(message, cause);

	}

	public RpcGenerateException(String message) {
		super(message);

	}

	public RpcGenerateException(Throwable cause) {
		super(cause);

	}

}
