package com.skyline.courier.rpc.exception;

public class NoSuchServiceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 883013250261263120L;

	public NoSuchServiceException() {
		super();
	}

	public NoSuchServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoSuchServiceException(String message) {
		super(message);
	}

	public NoSuchServiceException(Throwable cause) {
		super(cause);
	}

}
