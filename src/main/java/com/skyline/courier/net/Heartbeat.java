package com.skyline.courier.net;

import java.io.Serializable;

public final class Heartbeat implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7538885016167488869L;

	public static final byte[] BYTES = new byte[0];

	private static Heartbeat instance = new Heartbeat();

	public static Heartbeat getHeartbeat() {
		return instance;
	}

	private Heartbeat() {
	}
}
