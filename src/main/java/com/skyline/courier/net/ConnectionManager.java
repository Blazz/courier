package com.skyline.courier.net;

import java.net.SocketAddress;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ConnectionManager {
	private ConcurrentHashMap<SocketAddress, Connection> disconnAddressList = new ConcurrentHashMap<SocketAddress, Connection>();
	private ConcurrentHashMap<SocketAddress, Connection> connectedAddressList = new ConcurrentHashMap<SocketAddress, Connection>();

	public Set<SocketAddress> getConnectedAddress() {
		return connectedAddressList.keySet();
	}

	public void addConnected(SocketAddress address, Connection connection) {
		connectedAddressList.put(address, connection);
	}

	public Connection removeConnected(SocketAddress address) {
		return connectedAddressList.remove(address);
	}
	
	public Connection getConnection(SocketAddress address) {
		return connectedAddressList.get(address);
	}

	public Set<SocketAddress> getDisconnectAddress() {
		return disconnAddressList.keySet();
	}

	public void addDisconnectAddress(SocketAddress address, Connection connection) {
		disconnAddressList.put(address, connection);
	}


	public Connection removeDisconnect(SocketAddress address) {
		return disconnAddressList.remove(address);
	}

	public Connection getDisconnect(SocketAddress address) {
		return disconnAddressList.get(address);
	}
}
