package com.skyline.courier.net;

public class EndPoint {
	private Boolean tcpNoDelay = true;
	private Boolean keepAlive = true;
	private Boolean reuseAddress = true;
	
	/**
	 * 建立连接超时时间(毫秒), default is.30000
	 */
	private Integer connectTimeout = 30000;

	/**
	 * 写空闲时间(秒), default is 10.
	 */
	private Integer writeIdleTime = 0;
	/**
	 * 读空闲时间(秒), default is 30.
	 */
	private Integer readIdleTime = 0;
	
	private Serializer serializer = new ObjectSerializer();

	public Boolean getTcpNoDelay() {
		return tcpNoDelay;
	}

	public void setTcpNoDelay(Boolean tcpNoDelay) {
		this.tcpNoDelay = tcpNoDelay;
	}

	public Boolean getKeepAlive() {
		return keepAlive;
	}

	public void setKeepAlive(Boolean keepAlive) {
		this.keepAlive = keepAlive;
	}

	public Boolean getReuseAddress() {
		return reuseAddress;
	}

	public void setReuseAddress(Boolean reuseAddress) {
		this.reuseAddress = reuseAddress;
	}

	public Integer getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(Integer connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public Integer getWriteIdleTime() {
		return writeIdleTime;
	}

	public void setWriteIdleTime(Integer writeIdleTime) {
		this.writeIdleTime = writeIdleTime;
	}

	public Integer getReadIdleTime() {
		return readIdleTime;
	}

	public void setReadIdleTime(Integer readIdleTime) {
		this.readIdleTime = readIdleTime;
	}

	public Serializer getSerializer() {
		return serializer;
	}
	
	public void setSerializer(Serializer serializer) {
		this.serializer = serializer;
	}
}
