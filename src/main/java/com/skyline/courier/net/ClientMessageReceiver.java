package com.skyline.courier.net;

import java.util.Map;

public class ClientMessageReceiver {
	@SuppressWarnings("unchecked")
	public void onMessageReceive(TransferBean response, Map<Long, SendFuture<?>> futures) {
		SendFuture<Object> future = (SendFuture<Object>) futures.remove(response.getSeq());
		if (future != null) {
			Object target = response.getTarget();
			
			if(target == null) {
				future.setResult(target);
			} else if(target instanceof Throwable) {
				future.setCause((Throwable) target);
			} else {
				future.setResult(target);
			}
		}
	}
}
