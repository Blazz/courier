package com.skyline.courier.net;

/**
 * 消息序列化接口
 * 
 * @author wuqh
 * 
 */
public interface Serializer {

	public byte[] serialize(Object object) throws Exception;

	public Object deserialize(byte[] bytes) throws Exception;

}
