﻿package com.skyline.courier.net;

/**
 * 流量控制器
 * 
 * @author wuqh
 *
 */
public interface FlowManager {

	public void acquire() throws InterruptedException;

	public void acquire(int permits) throws InterruptedException;

	public boolean acquire(int permits, int timeout);

	public void release();

	public void release(int permits);

	public int getAvailable();

	public void setThreshold(int newThreshold);

	public int getThreshold();

}
