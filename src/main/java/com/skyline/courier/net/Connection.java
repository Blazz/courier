package com.skyline.courier.net;

import java.net.SocketAddress;

public interface Connection {
	<I, O> O send(I input) throws Throwable;

	boolean isOpen();

	void close();
	
	void reconnect();
	
	void setStatistic(Statistic statistic);
	
	void setConnectionManager(ConnectionManager connectionManager);
	
	void setFlowThreshold(int threshold);
	
	void setRemoteAddress(SocketAddress address);
	
	void setCompeleteTimeout(Integer completeTimeout);
	
	void setFlowTimeout(Integer flowTimeout);
}
