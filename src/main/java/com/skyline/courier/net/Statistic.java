package com.skyline.courier.net;

import java.io.Serializable;
import java.net.SocketAddress;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class Statistic {
	private ConcurrentHashMap<SocketAddress, StatisticInfo> msgStatistic = new ConcurrentHashMap<SocketAddress, StatisticInfo>();
	private ConcurrentHashMap<SocketAddress, StatisticInfo> heartbeatStatistic = new ConcurrentHashMap<SocketAddress, StatisticInfo>();

	public StatisticInfo getMsgStatistic(SocketAddress address) {
		StatisticInfo info = msgStatistic.get(address);
		if (info == null) {
			info = new StatisticInfo();
			msgStatistic.put(address, info);
		}
		return info;
	}

	public StatisticInfo getHeartBeatStatistic(SocketAddress address) {
		StatisticInfo info = heartbeatStatistic.get(address);
		if (info == null) {
			info = new StatisticInfo();
			heartbeatStatistic.put(address, info);
		}
		return info;
	}
	
	public void sentHeartBeat(SocketAddress address, long time) {
		long now = System.currentTimeMillis();
		getHeartBeatStatistic(address).getSentNum().incrementAndGet();
		getHeartBeatStatistic(address).setLastestSent(now);
	}
	
	public void receivedHeartBeat(SocketAddress address, long time) {
		long now = System.currentTimeMillis();
		getHeartBeatStatistic(address).getReceivedNum().incrementAndGet();
		getHeartBeatStatistic(address).setLastestReceived(now);
	}
	
	public void sentMsg(SocketAddress address, long time) {
		long now = System.currentTimeMillis();
		getMsgStatistic(address).getSentNum().incrementAndGet();
		getMsgStatistic(address).setLastestSent(now);
	}
	
	public void receivedMsg(SocketAddress address, long time) {
		long now = System.currentTimeMillis();
		getMsgStatistic(address).getReceivedNum().incrementAndGet();
		getMsgStatistic(address).setLastestReceived(now);
	}

	public static class StatisticInfo implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3223092213493717243L;
		private long lastestReceived;
		private long lastestSent;
		private AtomicLong receivedNum = new AtomicLong();
		private AtomicLong sentNum = new AtomicLong();

		public AtomicLong getReceivedNum() {
			return receivedNum;
		}

		public AtomicLong getSentNum() {
			return sentNum;
		}

		public long getLastestReceived() {
			return lastestReceived;
		}

		public void setLastestReceived(Long lastestReceived) {
			this.lastestReceived = lastestReceived;
		}

		public void setLastestReceivedIfLater(Long lastestReceived) {
			if (lastestReceived == null) {
				return;
			}
			if (this.lastestReceived < lastestReceived) {
				this.lastestReceived = lastestReceived;
			}
		}

		public Long getLastestSent() {
			return lastestSent;
		}

		public void setLastestSent(Long lastestSent) {
			this.lastestSent = lastestSent;
		}

		public void setLastestSentIfLater(Long lastestSent) {
			if (lastestSent == null) {
				return;
			}
			if (this.lastestSent < lastestSent) {
				this.lastestSent = lastestSent;
			}
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("StatisticInfo [lastestReceived=");
			builder.append(lastestReceived);
			builder.append(", lastestSent=");
			builder.append(lastestSent);
			builder.append(", receivedNum=");
			builder.append(receivedNum);
			builder.append(", sentNum=");
			builder.append(sentNum);
			builder.append("]");
			return builder.toString();
		}

	}
}
