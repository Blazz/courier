package com.skyline.courier.net;


public interface LoadBalancer {
	void setConnectionManager(ConnectionManager connectionManager);
	
	Connection next();
}
