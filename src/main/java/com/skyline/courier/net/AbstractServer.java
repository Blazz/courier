package com.skyline.courier.net;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractServer extends EndPoint implements Server {
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractServer.class);
	protected Handler handler;

	@Override
	public void bind(int port) {
		try {
			LOGGER.info("服务端[port:" + port + "]开始启动...");
			doBind(port);
			LOGGER.info("服务端[port:" + port + "]启动成功");
		} catch (Exception e) {
			LOGGER.info("服务端[port:" + port + "]启动失败", e);
			shutdown();
		}
	}

	@Override
	public void shutdown() {
		LOGGER.info("服务端开始关闭...");
		doShutdown();
		LOGGER.info("服务端关闭成功");
	}

	protected abstract void doBind(int port);

	protected abstract void doShutdown();

	@Override
	public void setHandler(Handler handler) {
		this.handler = handler;
	}

}
