package com.skyline.courier.net;

public interface Handler {
	<I, O> O process(I input) throws Exception;
}
