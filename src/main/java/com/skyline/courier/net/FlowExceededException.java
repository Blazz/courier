package com.skyline.courier.net;

public class FlowExceededException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 699010166219601874L;

	public FlowExceededException() {
		super();
	}

	public FlowExceededException(String message, Throwable cause) {
		super(message, cause);
	}

	public FlowExceededException(String message) {
		super(message);
	}

	public FlowExceededException(Throwable cause) {
		super(cause);
	}

}
