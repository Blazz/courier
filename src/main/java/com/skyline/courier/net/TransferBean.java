package com.skyline.courier.net;

import java.io.Serializable;

public class TransferBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2584336149863792494L;

	private long seq;

	private Object target;
	
	public long getSeq() {
		return seq;
	}
	
	public void setSeq(long seq) {
		this.seq = seq;
	}
	
	public Object getTarget() {
		return target;
	}
	
	public void setTarget(Object target) {
		this.target = target;
	}
}
