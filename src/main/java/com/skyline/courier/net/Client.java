package com.skyline.courier.net;

import java.io.IOException;
import java.net.SocketAddress;

public interface Client {
	void start();
	
	Connection connect(SocketAddress address) throws IOException;
	
	Connection connect() throws IOException;
	
	void setHosts(String hosts);
	
	void close();
}
