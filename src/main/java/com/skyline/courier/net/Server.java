package com.skyline.courier.net;

public interface Server {
	void bind(int port);
	
	void shutdown();
	
	void setHandler(Handler handler);
}
